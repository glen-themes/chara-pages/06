// if you're here i hope you learn something
// bc i sure as hell got no clue what im doing
$(document).ready(function(){
    /*------- PRELOAD SHIT -------*/
    $(".charalist a:first").addClass("active");
    $(".image-container").addClass("img-vignette");
    $(".info-text").css("opacity","1");
    
    /*------- LOAD NIER CURSOR SVG -------*/
    fetch('//ass-ets.glitch.me/nier-cursor.html')
    .then(data => {
      return data.text()
    })
    .then(data => {
      var divs = document.getElementsByClassName("curse");
        for (var i = 0; i < divs.length; i++){
            divs[i].innerHTML = data;
        }
    });
    
    window.onload = () => {
        $(".toplinks .curse svg > g").css("fill","var(--TopLinks-Accent-Color)");
        $(".charalist .curse svg > g").css("fill","var(--CharacterName-Accent-Color)");
        $(".bio-text table .curse svg > g").css("fill","var(--InfoBox-Bullet-Icon-Color)");
    }
    
    /*------- TOP LINKS HOVER BEHAVIOR -------*/
    $(".toplinks a").hover(function(){
        $(".highlight").removeClass("highlight").addClass("soup");
      }, function(){
        $(".soup").addClass("highlight").removeClass("soup");
      }
    );
    
    /*------- IMAGE CONTAINER BACKGROUND -------*/
    var bg = $(".image-container").css('background-image');
    bg = bg.replace('url("','').replace('")','');
    
    
    if (bg === 'https://www.toptal.com/designers/subtlepatterns/patterns/prism.png') {
        $(".image-container").css("background-size", "contain");
    }
    
    /*------- CHARACTER LIST SCROLLBAR -------*/
    if($(".charalist").height() < $(".anchovy").height()){
        $(".charalist").css("margin-right","calc(var(--InfoBox-Border-Size) * 5.5)")
    }
    
    $(".bio-text").each(function(){
        if($(this).height() < $(".info-text").height()){
            $(this).css("padding-right","calc(var(--InfoBox-Border-Size) * 3)");
        }
    });
    
    
    $(window).on("resize", function(){
    $(".bio-text").each(function(){
        if($(this).height() < $(".info-text").height()){
            $(this).css("padding-right","calc(var(--InfoBox-Border-Size) * 3)");
        }
        
        if($(this).height() > $(".info-text").height()){
            $(this).css("padding-right","0");
        }
    });
    });
    
    /*------- CHARACTER NAMES HOVER -------*/
    $(".charalist a").hover(function(){
        $(".active").removeClass("active").addClass("gay");
      }, function(){
        $(".gay").addClass("active").removeClass("gay");
      }
    );
    
    /*------- GET SWITCH FADE SPEED -------*/
    var speed = parseInt(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Switch-Fade-Speed"));
    
    /*------- CHARACTER NAMES ON CLICK -------*/
    /* hide everything, then show the clicked character */
    $(".charalist a").click(function(){
        
        $(".info-text").css("opacity","0");
        
    	var index = $(this).index();
    	
    	var getID = $(this).attr("character-id");
    	var index = '[character-id="' + getID + '"]'
    	
    	$(".charalist a").removeClass("active gay");
        $(this).filter(index).addClass("active");
    	
    	/*------*/
    	
        setTimeout(function(){
            $(".title-text span").hide();
        }, speed/2);
        
        setTimeout(function(){
            $(".title-text span").filter(index).show();
        }, speed/2);
    	
    	/*-----*/
    	
    	if($(".character-image").filter(index).is(":hidden")){
    	    $(".character-image").fadeOut(speed);
    	}
        	
    	if($(".character-image").has("[character-id]")){
        	$(".character-image").filter(index).delay(speed).fadeIn(speed);
    	}
    	
        /*
            here i basically used css to animate the fade and
            wrote the jquery as hard show/hide to match it
            
            bc .fadeIn() & .fadeOut() don't work on scrollbars
            so if there's a change in height, it either flickers
            or suddenly jumps and that kinda annoys me lol
            
            who even reads this tho fr
        */
        
        if($(".bio-text").filter(index).is(":hidden")){
            $(".info-text").removeClass("opacity");
            
            setTimeout(function(){
                $(".bio-text").hide();
            },speed);
            
            /* ---- */
            
            setTimeout(function(){
                $(".bio-text").filter(index).show();
            },speed);
            
            setTimeout(function(){
                $(".info-text").addClass("opacity");
            },speed);
        }
        
        if ($(".character-image").filter(index).length == 0) {
        	$(".character-image").fadeOut(speed);
        	$(".info-text").removeClass("opacity");
            
            setTimeout(function(){
                $(".bio-text").hide();
            },speed);
        }
    });//end click
    
    /*------- CHARACTER IMAGE ASSETS -------*/
    $(".character-image").each(function(){
        
         if($(this).attr("top") === ""){
             $(this).css("top","0");
         }
         
         if($(this).attr("bottom") === ""){
             $(this).css("bottom","0");
         }
         
         if($(this).attr("left") === ""){
             $(this).css("left","0");
         }
         
         if($(this).attr("right") === ""){
             $(this).css("right","0");
         }
         
         if($(this).attr("type") !== "full_sized"){
             if($(this).attr("height")){
                 $(this).css("max-height","none")
             }
         }
         
        var getTop = $(this).attr("top");
        $(this).css("margin-top",getTop);
        
        var getBot = $(this).attr("bottom");
        $(this).css("margin-bottom",getBot);
        
        var getLeft = $(this).attr("left");
        $(this).css("margin-left",getLeft);
        
        var getRight = $(this).attr("right");
        $(this).css("margin-right",getRight);
        
        /* if image is full sized, fill the container */
        if($(this).attr("type") == "full_sized"){
             $(this).css("max-height","none");
             
             /* if image is landscape, fill container */
             if($(this).width() > $(this).height()){
                 $(this).css("height","100%")
             }
             
             /* in case landscape doesn't fill */
             if($(this).width() < $(".image-container").width()){
                 $(this).css({"position":"absolute",
                              "height":"auto",
                              "margin-left":"0",
                              "margin-right":"0",
                              "width":"100%"
                 });
             }
             
             /* if image is portrait, fill container */
             if($(this).height() > $(this).width()){
                 $(this).css("width","100%")
             }
             
             /* in case portrait doesn't fill */
             if($(this).height() < $(".image-container").height()){
                 $(this).css({"position":"absolute",
                              "width":"auto",
                              "margin-top":"0",
                              "margin-bottom":"0",
                              "height":"100%"
                 });
             }
         }//end: if full_sized img
         
         $(".character-image:first").fadeIn(speed);
    });//end image assets
    
    /*------- SPECIAL TEXT ADJUSTMENTS -------*/
    $(".bio-text special").each(function(){
        var tuna = $(this).css("background-color"),
            egg = $(".oilsardine").css("background-color");
        
        if(tuna == egg){
            $(this).css({"padding":"0",
                         "margin":"0"
                       });
        }
        
        if($(this).text().endsWith(":")){
            var pr = $(this).css("padding-right"),
                hoo = parseInt(pr) * 0.69;
                
            $(this).css("padding-right",hoo + "px")
        }
        
        if ($(this).prev().length === 0) {
            $(this).css("margin-left","0")
            
        }
    });//end special text
    
});//end docready
