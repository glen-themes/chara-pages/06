// if you're here i hope you learn something
// bc i sure as hell got no clue what im doing

document.addEventListener("DOMContentLoaded", () => {
    /*------- PRELOAD SH!T -------*/
    (document.querySelector(".charalist a") || null).classList.add("active")
    document.querySelectorAll(".image-container")?.forEach(x => x.classList.add("img-vignette"))
    document.querySelectorAll(".info-text")?.forEach(x => x.style.opacity = 1)
    
    /*------- LOAD NIER CURSOR SVG -------*/
    fetch("//glen-themes.gitlab.io/chara-pages/06/cursor.svg")
    .then(r => r.text())
    .then(r => {
      document.querySelectorAll(".curse")?.forEach(i => i.innerHTML = r)
      
      document.querySelectorAll(".toplinks .curse svg > g")?.forEach(g => {
        g.style.fill = "var(--TopLinks-Accent-Color)"
      })
      
      document.querySelectorAll(".charalist .curse svg > g")?.forEach(g => {
        g.style.fill = "var(--CharacterName-Accent-Color)"
      })
      
      document.querySelectorAll(".bio-text table .curse svg > g")?.forEach(g => {
        g.style.fill = "var(--InfoBox-Bullet-Icon-Color)"
      })
    })
    .catch(error => console.error(error))
    
    /*------- IMAGE CONTAINER BACKGROUND -------*/
    document.querySelectorAll(".image-container")?.forEach(imgcont => {
      let bg = getComputedStyle(imgcont).getPropertyValue("background-image");
      bg = bg.replace('url("', '').replace('")', '');
      if(bg.indexOf("toptal.com/designers/subtlepatterns/patterns/prism.png") > -1 || bg.indexOf("rhizo.gitlab.io/nier0/subtlepatterns_prism.png") > -1){
        imgcont.style.backgroundSize = "contain"
      }
    })
    
    /*------- CHARACTER LIST SCROLLBAR -------*/
    Promise.all(Array.from(document.images).filter(img => !img.complete).map(img => new Promise(resolve => { img.onload = img.onerror = resolve; }))).then(() => {
      // character list scrollbar
      let charalist = document.querySelector(".charalist")
      let anchovy = document.querySelector(".anchovy")
      if(charalist.offsetHeight < anchovy.offsetHeight){
        charalist.style.marginRight = "calc(var(--InfoBox-Border-Size) * 5.5)"
      }
    })
    
    // character bio scrollbar
    let infotext = document.querySelector(".info-text")
    document.querySelectorAll(".bio-text")?.forEach(bio => {
      if(bio.offsetHeight < infotext.offsetHeight){
        bio.style.paddingRight = "calc(var(--InfoBox-Border-Size) * 3)"
      }
    })
    
    /*------- CHARACTER IMAGE ASSETS -------*/
    document.querySelectorAll(".character-image")?.forEach((img,imgIndex) => {
      if(img.getAttribute("top") === ""){
        img.style.top = 0
      }
      
      if(img.getAttribute("right") === ""){
        img.style.right = 0
      }
      
      if(img.getAttribute("bottom") === ""){
        img.style.bottom = 0
      }
      
      if(img.getAttribute("left") === ""){
        img.style.left = 0
      }
      
      if(img.getAttribute("type") !== "full_sized"){
        if(img.matches("[height]")){
          img.style.maxHeight = "none"
        }
      }
      
      let getTop = img.getAttribute("top")
      img.style.marginTop = getTop
      
      let getRight = img.getAttribute("right")
      img.style.marginRight = getRight
      
      let getBot = img.getAttribute("bottom")
      img.style.marginBottom = getBot
      
      let getLeft = img.getAttribute("left")
      img.style.marginLeft = getLeft
      
      // if image is full sized, fill the container
      if(img.getAttribute("type") == "full_sized"){
        img.style.height = "100%"
        
        // stuff to do after it loads
        new Promise((resolve, reject) => {
          let img_ = new Image();
          img_.addEventListener("load", () => resolve(img_));
          img_.addEventListener("error", (err) => reject(err));
          img_.src = img.src;
        })
        .then(img => {
          // if image is landscape, fill container
          if(img.offsetWidth > img.offsetHeight){
            img.style.height = "100%"
          }
          
          // in case landscape doesn't fill
          if(img.offsetWidth < (document.querySelector(".image-container").offsetWidth || 0)){
            img.style.position = "absolute"
            img.style.height = "auto"
            img.style.marginLeft = 0
            img.style.marginRight = 0
            img.style.width = "100%"
          }
          
          // if image is portrait, fill container
          if(img.offsetHeight > img.offsetWidth){
            img.style.width = "100%"
          }
          
          // in case portrait doesn't fill
          if(img.offsetHeight < (document.querySelector(".image-container").offsetWidth || 0)){
            img.style.position = "absolute"
            img.style.width = "auto"
            img.style.marginTop = 0
            img.style.marginBottom = 0
            img.style.height = "100%"
          }
          
          if(imgIndex == 0){
            if(typeof $ == "function"){
              var speed = parseInt(getComputedStyle(document.documentElement).getPropertyValue("--Switch-Fade-Speed"));
              $(".character-image:first").fadeIn(speed);
            }
          }
        })//end img loaded
        .catch(err => console.error(err));
        
      }//end: if full_sized img
    })//end .character-image each
    
    /*------- SPECIAL TEXT ADJUSTMENTS -------*/
    document.querySelectorAll(".bio-text special")?.forEach(special => {
      let tuna = getComputedStyle(special).getPropertyValue("background-color"),
          egg = getComputedStyle(document.querySelector(".oilsardine") || document.body).getPropertyValue("background-color")
  
      if(tuna == egg){
        special.style.padding = 0
        special.style.margin = 0
      }
      
      if(special.textContent.endsWith(":")){
        let pr = getComputedStyle(special).getPropertyValue("padding-right")
        let hoo = parseInt(pr) * 0.69
        special.style.paddingRight = `${hoo}px`
      }
      
      if(!special.previousElementSibling){
        special.style.marginLeft = 0
      }
    })//end <special> each
  })//end DOMContentLoaded
  
  /*-------------------------*/
  
  $(document).ready(function(){
    /*------- TOP LINKS HOVER BEHAVIOR -------*/
    $(".toplinks a").hover(function(){
        $(".highlight").removeClass("highlight").addClass("soup");
      }, function(){
        $(".soup").addClass("highlight").removeClass("soup");
      }
    );
    
    /*------- CHARACTER NAMES HOVER -------*/
    $(".charalist a").hover(function(){
        $(".active").removeClass("active").addClass("gay");
      }, function(){
        $(".gay").addClass("active").removeClass("gay");
      }
    );
  
    /*------- GET SWITCH FADE SPEED -------*/
    var speed = parseInt(getComputedStyle(document.documentElement)
      .getPropertyValue("--Switch-Fade-Speed"));
  
    /*------- CHARACTER NAMES ON CLICK -------*/
    /* hide everything, then show the clicked character */
    $(".charalist a").click(function(){
  
      $(".info-text").css("opacity","0")
  
      var index = $(this).index();
  
      var getID = $(this).attr("character-id")
      var index = '[character-id="' + getID + '"]'
  
      $(".charalist a").removeClass("active gay")
      $(this).filter(index).addClass("active")
  
      /*------*/
  
      setTimeout(function(){
        $(".title-text span").hide();
      },speed / 2);
  
      setTimeout(function(){
        $(".title-text span").filter(index).show();
      },speed / 2);
  
      /*-----*/
  
      if($(".character-image").filter(index).is(":hidden")){
        $(".character-image").fadeOut(speed);
      }
  
      if($(".character-image").has("[character-id]")){
        $(".character-image").filter(index).delay(speed).fadeIn(speed);
      }
  
      /*
          here i basically used css to animate the fade and
          wrote the jquery as hard show/hide to match it
          
          bc .fadeIn() & .fadeOut() don't work on scrollbars
          so if there's a change in height, it either flickers
          or suddenly jumps and that kinda annoys me lol
          
          who even reads this tho fr
      */
  
      if($(".bio-text").filter(index).is(":hidden")){
        $(".info-text").removeClass("opacity")
  
        setTimeout(function(){
          $(".bio-text").hide();
        },speed);
  
        /* ---- */
  
        setTimeout(function(){
          $(".bio-text").filter(index).show();
        },speed);
  
        setTimeout(function(){
          $(".info-text").addClass("opacity")
        },speed);
      }
  
      if($(".character-image").filter(index).length == 0){
        $(".character-image").fadeOut(speed);
        $(".info-text").removeClass("opacity")
  
        setTimeout(function(){
          $(".bio-text").hide();
        },speed);
      }
    })//end click
  
  })//end docready